package logger

import (
	"os"

	"github.com/rs/zerolog"
)

// Log is a singleton logger.
var Log Logger = NewLogger()

// Logger is interface for logger that hides specific implementation details.
type Logger interface {
	// WithFields accepts varargs in form of key-value pairs,
	// e.g. WithFields("key_1", "value_2", "key_2", "value_2").
	WithFields(fields ...string) Logger

	// Debug logs message in debug level.
	Debug(message string, values ...interface{})

	// Info logs message in logger level.
	Info(message string, values ...interface{})

	// Warn logs message in warn level.
	Warn(message string, values ...interface{})

	// Error logs message in error level.
	Error(message string, values ...interface{})

	// Fatal logs message in fatal level and exits process with code 1.
	Fatal(message string, values ...interface{})
}

// JSONLogger is a default structured logger that implements Logger interface.
type JSONLogger struct {
	l zerolog.Logger
}

// NewLogger creates a new instance of JSONLogger.
func NewLogger() *JSONLogger {
	return NewLoggerLevel("") // default logger level is info
}

// NewLoggerLevel creates a new logger that's different from the standard logger
// so that it can maintain different logger levels from the global level.
func NewLoggerLevel(level string) *JSONLogger {
	var logLevel zerolog.Level

	switch level {
	case zerolog.DebugLevel.String():
		logLevel = zerolog.DebugLevel
	case zerolog.WarnLevel.String():
		logLevel = zerolog.WarnLevel
	case zerolog.ErrorLevel.String():
		logLevel = zerolog.ErrorLevel
	default:
		logLevel = zerolog.InfoLevel
	}

	log := zerolog.New(os.Stdout).
		Level(logLevel).
		With().Timestamp().Logger()
	return &JSONLogger{l: log}
}

// WithFields returns new JSONLogger with provided fields.
func (logger *JSONLogger) WithFields(fields ...string) Logger {
	if len(fields) == 0 {
		return &JSONLogger{l: logger.l.With().Logger()}
	}

	fieldsMap := make(map[string]interface{})

	var key string
	for i, f := range fields {
		if i%2 == 0 {
			key = f
			continue
		}
		fieldsMap[key] = f
	}

	if len(fields) > 0 {
		return &JSONLogger{l: logger.l.With().Fields(fieldsMap).Logger()}
	}
	return &JSONLogger{l: logger.l.With().Logger()}
}

// Debug logs message in debug level.
func (logger *JSONLogger) Debug(message string, values ...interface{}) {
	msg(logger.l.Debug(), message, values)
}

// Info logs message in logger level.
func (logger *JSONLogger) Info(message string, values ...interface{}) {
	msg(logger.l.Info(), message, values)
}

// Warn logs message in warn level.
func (logger *JSONLogger) Warn(message string, values ...interface{}) {
	msg(logger.l.Warn(), message, values)
}

// Error logs message in error level.
func (logger *JSONLogger) Error(message string, values ...interface{}) {
	msg(logger.l.Error(), message, values)
}

// Fatal logs message in fatal level and exits process with code 1.
func (logger *JSONLogger) Fatal(message string, values ...interface{}) {
	msg(logger.l.Fatal(), message, values)
}

func msg(event *zerolog.Event, message string, values []interface{}) {
	if values == nil || len(values) == 0 {
		event.Msg(message)
		return
	}
	event.Msgf(message, values...)
}
